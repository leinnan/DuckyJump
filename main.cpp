#include "config.h"
#include "Engine.h"
#include <cstdlib>
#include <ctime>

int main()
{
    srand( time( NULL ) );
    Engine engine;
    engine.run();

    return 0;
}
